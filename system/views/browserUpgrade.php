<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>浏览器升级提示</title>
	<style>
	body{ font-family: 'Microsoft Yahei'; }
	</style>
</head>
<body>
	<h2>很遗憾，IBOS 暂时不支持 IE 8 以下的浏览器（以后也不打算支持）</h2>
	<h3>为了更好地使用本系统，你可以选择</h3>
	<ul>
		<li>
			<a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">升级浏览器</a>
		</li>
		<li>
			安装强大、好用的标准浏览器
			<div>	
				<a href="http://www.google.cn/chrome/browser/desktop/index.html">Chrome</a>
				<a href="http://www.firefox.com.cn/download/">Firefox</a>
				<a href="http://www.opera.com/zh-cn">Opera</a>
				<a href="http://www.apple.com/cn/safari/">Safari</a>
			</div>
		</li>
		<li>
			<a href="http://www.ibos.com.cn/file/99">如果您还是习惯使用当前的浏览器,请安装 Chrome 框架</a>
		</li>
	</ul>
	<h2></h2>
	<h3>如果你的浏览器版本高于 IE 8，请按以下步骤设置（以 IE10 为例）</h3>
	<ol>
		<li>按下键盘F12</li>
		<li>
			修改 “浏览器模式：IE10 兼容性视图” 为 “浏览器模式 Internet Explorer 10(B)”
			<br>
			<br>
			<img src="data/login/ie10_兼容.png">
			<br>
			<br>
			<img src="data/login/ie10.png">
		</li>
		<li>
			<a href="/">返回首页</a>
		</li>
	</ol>
</body>
</html>